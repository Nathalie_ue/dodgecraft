﻿using Core;
using GameFlowSystem.States;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using PlayerSystem;
using ScoreSystem;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using CurrencySystem.View;
using ItemSystem;
using UnityEngine.Advertisements;

namespace GameFlowSystem
{
    /// <summary>
    /// 	State Machine controlling the game flow. Implements state pattern.
    /// </summary>
    public class GameFlow : Singleton<GameFlow>
    {
        #region Serialize Fields

        [SerializeField] private MainMenuState _mainMenuState;
        [SerializeField] private PauseGameState _pauseGameState;
        [SerializeField] private StartGameState _startGameState;
        [SerializeField] private PlayGameState _playGameState;
        [SerializeField] private GameOverState _gameOverState;
        [SerializeField] private SaveState _saveState;
        [SerializeField] private LoadState _loadState;

        //The menu buttons
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _returnButton;
        [SerializeField] private Button _signInButton;
        [SerializeField] private Button _signOutButton;
        [SerializeField] private Button _leaderboardButton;
        [SerializeField] private Button _goBackButton;
        [SerializeField] private Button _globalLeaderboardButton;
        [SerializeField] private Button _nativeLeaderboardButton;
        [SerializeField] private Button _achievementButton;
        [SerializeField] private GameObject _settingMenu;
        [SerializeField] private GameObject _leaderboardMenu;

        #endregion

        #region Private Fields

        private IGameState _currentState;
        private PlayerController _player;
        private bool _socialPlatformActivated;
        private bool _settingIsActive;

        #endregion

        #region Properties

        public GameOverState GameOverState => _gameOverState;
        public bool IsGameRunning => _currentState.Equals(PlayGameState);
        public LoadState LoadState => _loadState;
        public MainMenuState MainMenuState => _mainMenuState;
        public PauseGameState PauseGameState => _pauseGameState;
        public PlayerController Player => _player;
        public PlayGameState PlayGameState => _playGameState;
        public SaveState SaveState => _saveState;
        public StartGameState StartGameState => _startGameState;
        public GameOverCoinUI _gameOverCoinUI;
        public LeaderboardScoreData leaderboardScoreData;

        #endregion

        #region Achievement Variables

        private bool _achievementLocked = false;
        private bool _usedTheShield = false;
        private bool _usedTheMagnet = false;
        private bool _usedTheShockWave = false;
        private float _internalTimer = 0f;
        private Vector3 _startingPosition;
        private MeshFilter _playerHatMesh;

        public Mesh _cowboyHatMesh;
        public Item _shieldItem;
        public Item _magnetItem;
        public Item _shockWaveItem;
        public Material _diamondAmour;

        #endregion

        #region Unity methods

        protected override void Awake()
        {
            base.Awake();

            _internalTimer = 0f;

            _player = FindObjectOfType<PlayerController>();

            _settingsButton.onClick.AddListener(OnClickSettings);
            _returnButton.onClick.AddListener(OnClickReturn);
            _signInButton.onClick.AddListener(OnClickSignIn);
            _signOutButton.onClick.AddListener(OnClickSignOut);
            _leaderboardButton.onClick.AddListener(OnClickLeaderboard);
            _goBackButton.onClick.AddListener(OnClickReturn);
            _globalLeaderboardButton.onClick.AddListener(OnGlobalLeaderboardButton);
            _nativeLeaderboardButton.onClick.AddListener(OnNativeLeaderboardButton);
            _achievementButton.onClick.AddListener(OnClickAchievementButton);

            _leaderboardMenu.SetActive(false);
            _settingMenu.SetActive(false);
            _settingIsActive = false;

            _startingPosition = _player.transform.position;

            _currentState = LoadState;
            _currentState.StateEnter();

            Time.timeScale = 0f;

#if UNITY_ANDROID 
            //logging in
            if (!_socialPlatformActivated)
            {
                PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
                PlayGamesPlatform.InitializeInstance(config);
                PlayGamesPlatform.Activate();
                _socialPlatformActivated = true;

                // authenticate user:
                PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce,
                    (result) =>
                    {                        
                        _currentState = LoadState;
                        _currentState.StateEnter();
                    });
            }
            else
            {
                _currentState = LoadState;
                _currentState.StateEnter();
            }

            //Code needed to commented out due to a Nullreference that I couldn't fix
            //ShowSavedGameUI();            
#endif
        }

        private void Update()
		{
			if (_currentState == null)
			{
				return;
			}

			// state pattern - update current state, hceck if it needs to be switched. Exit current one and enter new one if required.
			IGameState nextState = _currentState.StateUpdate();
			if (nextState != _currentState)
			{
				_currentState.StateExit();
				_currentState = nextState;
				_currentState.StateEnter();
			}

            //After Gameover the leaderboard will be shown
            if(_gameOverState)
            {
                LoadLeaderboard();
            }

            //You can unlock achievements only while the game is running
            if(IsGameRunning)
            {
                AchievementsToUnlock();
            }
            else if(_mainMenuState)
            {
                //player buys and wears a cowboy hat
                _playerHatMesh = _player.GetComponentInChildren<MeshFilter>();

                if (_playerHatMesh.mesh == _cowboyHatMesh)
                {
                    UnlockAchievement("CgkI1_POkv8JEAIQBg");

                    Debug.Log("player buys and wears a cowboy hat");
                }
            }
        }

        #region Saved Games
        public void ShowSavedGameUI()
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.ShowSelectSavedGameUI("Select saved game",
               10,
               true,
               true,
               OnSavedGameSelected
            );
        }

        private void OnSavedGameSelected(SelectUIStatus status, ISavedGameMetadata game)
        {
            if (status == SelectUIStatus.SavedGameSelected)
            {
                OpenSavedGame(game.Filename);
            }
            else
            {
                // handle error
                Debug.Log("Failed to open saved game");
            }
        }

        void OpenSavedGame(string filename)
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.OpenWithAutomaticConflictResolution(
               filename,
               DataSource.ReadCacheOrNetwork,
               ConflictResolutionStrategy.UseLongestPlaytime,
               OnSavedGameOpened
               );
        }

        public void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                LoadGameData(game);
            }
            else
            {
                // handle error
                Debug.Log("Failed to open saved game");
            }
        }

        void SaveGame(ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime)
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

            SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
            builder = builder
               .WithUpdatedPlayedTime(totalPlaytime)
               .WithUpdatedDescription("Saved game at " + DateTime.Now);

            Texture2D savedImage = getScreenshot();

            if (savedImage != null)
            {
                // savedImage is an instance of Texture2D
                // call getScreenshot() to set savedImage
                byte[] pngData = savedImage.EncodeToPNG();
                builder = builder.WithUpdatedPngCoverImage(pngData);
            }

            SavedGameMetadataUpdate updatedMetadata = builder.Build();
            savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
        }

        public void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                // handle reading or writing of saved game.
                LoadGameData(game);
            }
            else
            {
                // handle error
                Debug.Log("failed to writing saved game");
            }
        }

        public Texture2D getScreenshot()
        {
            // Create a 2D texture that is 1024x700 pixels from which the PNG will be
            // extracted
            Texture2D screenShot = new Texture2D(1024, 700);

            // Takes the screenshot from top left hand corner of screen and maps to top
            // left hand corner of screenShot texture
            screenShot.ReadPixels(new Rect(0, 0, Screen.width, (Screen.width / 1024) * 700), 0, 0);
            return screenShot;
        }

        void LoadGameData(ISavedGameMetadata game)
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
        }

        public void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                // handle processing the byte array data
                BinaryToJson(data);
            }
            else
            {
                // handle error    
                Debug.Log("failed to delete saved game");
            }
        }

        public void JsonToBinary()
        {
            string json = "{Score:10}";
            byte[] bytes = Encoding.ASCII.GetBytes(json);
        }

        public void BinaryToJson(byte[] bytes)
        {
            string str = Encoding.ASCII.GetString(bytes);
        }

        void DeleteGameData(string filename)
        {
            // Open the file to get the metadata.
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.OpenWithAutomaticConflictResolution(
               filename,
               DataSource.ReadCacheOrNetwork,
               ConflictResolutionStrategy.UseLongestPlaytime,
               DeleteSavedGame);
        }

        public void DeleteSavedGame(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
                savedGameClient.Delete(game);
            }
            else
            {
                // handle error
                Debug.Log("failed to delete saved game");
            }
        }
        #endregion

        #region Authentication
        public void OnClickSettings()
        {
            _settingMenu.SetActive(true);
            _settingIsActive = true;
        }

        public void OnClickSignIn()
        {
#if UNITY_ANDROID
            //logging in           

            // authenticate user:
            PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptAlways,
                (result) =>
                {
                    _currentState = LoadState;
                    _currentState.StateEnter();
                });

            Debug.Log("Sign In");
#endif
        }

        public void OnClickSignOut()
        {
#if UNITY_ANDROID
            //logging out

            // authenticate user:
            PlayGamesPlatform.Instance.SignOut();

            Debug.Log("Sign Out");
#endif
        }
        #endregion

        public void OnClickReturn()
        {
            if(_settingIsActive)
            {
                _settingMenu.SetActive(false);
                _settingIsActive = false;
            }
            else
            {
                _leaderboardMenu.SetActive(false);
            }
            
        }

        #region Leaderboard
        public void OnClickLeaderboard()
        {
            _leaderboardMenu.SetActive(true);

            LoadLeaderboard();
        }

        public void LoadLeaderboard()
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI();

            Social.ReportScore(ScoreManager.Instance.HighScore,
                GPGSIds.leaderboard_global,
                (res) =>
                {
                    PlayGamesPlatform.Instance.LoadScores(
                        GPGSIds.leaderboard_global,
                        LeaderboardStart.PlayerCentered,
                        10,
                        LeaderboardCollection.Public,
                        LeaderboardTimeSpan.AllTime,
                        FillWithData
                        );
                });
        }        

        private void FillWithData(LeaderboardScoreData data)
        {
            for (int i = 0; i < data.Scores.Length; i++) //Loop through the data
            {
                string rank = data.Scores[i].rank.ToString();
                string name = data.Scores[i].userID;
                string score = data.Scores[i].value.ToString();
            }                       

            //Debug.Log(data.Valid);
            //Debug.Log(data.Id);
            //Debug.Log(data.PlayerScore);
            //Debug.Log(data.Scores);
            //Debug.Log(data.ToString());
        }

        private void OnGlobalLeaderboardButton()
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI();

            Social.ReportScore(ScoreManager.Instance.HighScore,
                GPGSIds.leaderboard_global,
                (res) =>
                {
                    PlayGamesPlatform.Instance.LoadScores(
                        GPGSIds.leaderboard_global,
                        LeaderboardStart.PlayerCentered,
                        10,
                        LeaderboardCollection.Social,
                        LeaderboardTimeSpan.AllTime,
                        FillWithData
                        );
                });
        }

        private void OnNativeLeaderboardButton()
        {
            LoadLeaderboard();
        }
        #endregion

        #region Achievements
        public void OnClickAchievementButton()
        {
            Social.ShowAchievementsUI();
        }


        //Difference between ingame achievements and store achievements?
        public void AchievementsToUnlock()
        {
            //normal unlock achievements

            //player dies for the first time
            if(_gameOverState && !_achievementLocked)
            {
                UnlockAchievement("CgkI1_POkv8JEAIQAg");
                _achievementLocked = true;

                Debug.Log("Player died for the first time");
            }

            //player stands still for 10 seconds without losing a life
            if(_player.transform.position == _startingPosition)
            {
                _internalTimer += Time.realtimeSinceStartup;
                if(_internalTimer >= 10f)
                {
                    UnlockAchievement("CgkI1_POkv8JEAIQAw");

                    Debug.Log("player stands still for 10 seconds without losing a life");
                }
            }
            else
            {
                _internalTimer = 0f;
            }

            //player survives for at least 2 Minutes
            if (IsGameRunning)
            {
                _internalTimer += Time.realtimeSinceStartup;
                if (_internalTimer >= 120f)
                {
                    UnlockAchievement("CgkI1_POkv8JEAIQCw");

                    Debug.Log("player survives for at least 2 Minutes");
                }
            }
            else
            {
                _internalTimer = 0f;
            }

            //player loses a game solely by getting hit by oil lamps
            // > or = to 3 because the player has min 3 lifes but can take healthPacks to heal lifes
            if (_player.hitByLantern >= 3)
            {
                Debug.Log("Player has been hit >3 times");
                //once the gameover state is reached, the achievement will be unlocked
                if(_gameOverState)
                {
                    UnlockAchievement("CgkI1_POkv8JEAIQCQ");

                    Debug.Log("player loses a game solely by getting hit by oil lamps");
                }
            }

            //purchases an item with diamonds
            //kept the diamonds in-game but couldn't get diamond purchases working in time


            //buys and uses the shield     
            //This code actually just checks if the player owns the item and not if they use it
            //But I couldn't figure out how to use FinishItemUse() of the Item Script without changing it too much
            if (_player._currentItem == _shieldItem)
            {
                UnlockAchievement("CgkI1_POkv8JEAIQBA");

                Debug.Log("buys and uses the shield");
            }

            //buy and use all 3 powerups in one match
            //-> that doesn't work so I changed it to "buy all 3 powerups at least once"
            if (_player._currentItem == _shieldItem)
            {
                _usedTheShield = true;
            }
            else if (_player._currentItem == _shockWaveItem)
            {
                _usedTheShockWave = true;
            }
            else if(_player._currentItem == _magnetItem)
            {
                _usedTheMagnet = true;
            }

            if (_usedTheShield && _usedTheMagnet && _usedTheShockWave)
            {
                UnlockAchievement("CgkI1_POkv8JEAIQCA");

                Debug.Log("buy all 3 powerups at least once");
            }

            //buy the full diamond armour set
            if (_player.GetComponent<MeshRenderer>().material == _diamondAmour)
            {
                UnlockAchievement("CgkI1_POkv8JEAIQCg");

                Debug.Log("buy the full diamond armour set");
            }

            //incremental achievement
            //player dies with exactly 96 coins
            if (_gameOverCoinUI.coinsCollectedThisRun < 96)
            {
                IncrementAchievement("CgkI1_POkv8JEAIQBQ", 5);
                Debug.Log("player has collected less than 96 coins");
            }
            else if (_gameOverCoinUI.coinsCollectedThisRun == 96)
            {
                IncrementAchievement("CgkI1_POkv8JEAIQBQ", 100);
                Debug.Log("player dies with exactly 96 coins");
            }
        }
        
        public void UnlockAchievement(string achievementID)
        {
            Social.ReportProgress(achievementID, 100.0f, (success) =>
            {
                // handle success or failure​
                if (success)
                {
                    Debug.Log("unlocked Achievement");
                }
                else
                {
                    Debug.Log("failed to unlock achievement");
                }
            });
        }

        public void IncrementAchievement(string achievementID, int steps)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(achievementID, steps, (success) =>
            {
                // handle success or failure​
                if (success)
                {
                    Debug.Log("increment achievement");
                }
                else
                {
                    Debug.Log("failed to increment achievement");
                }
            });
        }
        #endregion

        #region Ads
        //all Ad-related code is completly commented out due to the fact, that Unity woudln't let me or my team members link the project to the Unity dashboard
        //to avoid any conflict I commented out all code and didn't include it in any further methods outside of this region

        //[SerializeField] private string _adUnitId = "Rewarded_Android";
        //private void ShowRewardedAd()
        //{
        //    Advertisement.Show(_adUnitId);
        //}
        #endregion

        #endregion
    }
}